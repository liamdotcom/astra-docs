# Multiple Inheritence

ASTRA supports code reuse through multiple inheritence, using the `extends` keyword.

## Single inheritence

Example:

```
agent Parent {

    module Console c;

    types parent_ont {
        formula belief_predicate(int);
    }

    rule +!validate(belief_predicate(int v)) {
        c.println("Default:"+v);
    }
}
```

```
agent Child extends Parent {

    rule +!main(list args) {
        //Set initial belief from arguments
        +belief_predicate(p.valueAsInt(args, 0));

        //Query belief
        query(belief_predicate(int v));

        //Goal: validate belief
        !validate(belief_predicate(v));
    }

    rule +!validate(belief_predicate(int v)) : v>1 {
        c.println(v+" is greater than 1");
    }
}
```

The child agent inherits modules, beliefs, goals and events from the parent agent. 

For example, the child parent inherits the formula `belief_predicate(int)` from the parent. It inherits the rule `!validate(belief_predicate(int v))`, and overrides it in certain circumstances (when `v>1`).
It also inherits the modules `Prelude` and `Console`.

```
agent Main {

    module System s;

    rule +!main(list args) {
        s.createAgent("child", "Child");
        s.setMainGoal("child", [1]);
    }

}
```

When the Child agent is created and run using the Main agent, the default rule for `!validate(...)` from Parent will be triggered, resulting in an output of 

`[child]Default:1`

If the argument passed in is changed to 5 the output will be:

`[child]5 is greater than 1`

## Multiple Inheritence

An agent can have multiple parents.

An additional parent agent:

```
agent Super {

   types super_ont {
       formula isSuper(boolean);
   }

   initial isSuper(true);

}
```

Modified child agent:

```
agent Child extends Parent, Super {

    rule +!main(list args) {
        //Set initial belief from arguments
        +belief_predicate(p.valueAsInt(args, 0));

        //Query belief
        query(belief_predicate(int v));

        //Goal: validate belief
        !validate(belief_predicate(v));

        //Query belief
        query(isSuper(true));
        c.println("agent is super agent");
    }

    rule +!validate(belief_predicate(int v)) : v>1 {
        c.println(v+" is greater than 1");
    }
}
```

The output of the program (passing in 1 from the Main agent) will be:

```
[child]Default:1
[child]agent is super agent
```