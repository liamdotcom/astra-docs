#Basic Language Examples#

This section of the guide presents simple ASTRA programs that illustrate how basic AgentSpeak(L) concepts are realised in ASTRA.

##Hello World##

**IDEA:**
Declare an initial goal that is handled by a single rule. The behaviour implemented in this rule is to display the text “Hello World” to the system console.

**CODE:**

```
agent Hello {
    module Console C;

    initial !init();

    rule +!init() {
        C.println("Hello World");
    }
}
```

**OUTPUT:**

```
[main]Hello World
```

##Terminal Hello World##

**IDEA:**

A variation on Hello World in which the agent terminates the program after it has printed out “Hello World”.

This is achieved through the use of the exit() action which is part of the astra.lang.System module.

**CODE:**

```
agent Hello {
    module Console C;
    module System S;
    initial !init();

    rule +!init() {
        C.println("Hello World");
        S.exit();
    }
}
```

**OUTPUT:**

```
[main]Hello World
```

_NOTE: The program terminates now after the text is output_
