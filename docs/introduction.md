# 1. Introduction and Overview

## 1.1 Team

The AgentSpeak Teleo-Reactive Agent (ASTRA) Programming Language is an open source Agent-Oriented Programming (AOP) language developed by researchers at University College Dublin. ASTRA is free to use under the [MIT License](https://gitlab.com/astra-language/astra-core/-/blob/master/LICENSE).

## 1.2 What is Agent-Oriented Programming?

Agent-Oriented Programming is a programming paradigm proposed in 1992 by Yoav Shoham of Stanford University.  It is centered around the creation of distributed systems comprised of intelligent entities known as *agents*.  These agents maintain an internal state that consists of various mental qualities, such as beliefs, desires, goals and intentions. Representation of this internal state varies across the different AOP languages that have been developed, but largely, they are modelled as sets of first-order logic formulae.  The dynamics of the mental state is governed by an associated set of rules that are programmed by the developer of the agent. The format of the rules is dictated by the set of mental qualities chosen to form the intenal (mental) state.

A common architecture that has emerged across AOP research is the Belief-Desire-Intention (BDI) architecture.  In this architecture, beliefs represent the current state of the agent, desires represent the ideal (and possibly inconsistent) future state of the agent, and intentions represent the subset of the agents desires that it has committed to trying to achieve.  Agents achieve their intentions by performing actions. The idea here is that the agent acts in order to transform their beliefs so that it matches its desires. That is, a desire is fulfilled when the agent believes it to be true.  This becomes more complicated by the fact that desires can often be inconsistent, for example, an agent may have a desire to be in two different locations, but can only be in one of those locations at any time.

Typically, agents are viewed as being situated in some environment, which the agent is able to sense and act.  Sensing involves the update of the agents beliefs (current state) to reflect any changes in that environment. The agent performs this sensing activity as part of a larger *reasoning cycle*, which also includes deliberation and acting.  Deliveration is the activity managing the agents intentions (what desires the agent is trying to achieve) and selecting the next action to be performed based on those intentions.  Acting is then the execution of the selected action either internally within the agent or via the associated environment.

How deliberation occurs varies depending on the design of each Agent Programming Language.


## 1.3 The ASTRA Approach

ASTRA is an Agent Programming Language for people who wish to create intelligent distributed/concurrent systems that are built on the Java platform. It is designed to be easy to learn and familiar to developers who are experienced in using mainstream Object-Oriented Programming Languages.

This has been achieved by designing ASTRA to be closely integrated with and influenced by Java. It is strongly typed and supports reuse through multiple inheritance.  Some of the main design decisions behind ASTRA have been made in an attempt to reduce the complexity of writing and running programs. In terms of writing ASTRA code, we have introduced a number of standard programming constructs, such as if statements, while loops, local variables, and assignment. We have also put a lot of effort into writing a compiler that is able to catch as many bugs as possible before you run the code.

In terms of running ASTRA code, we have created an approach where the code can be run directly without the need for additional configuration files. This makes running an ASTRA program the same as running any program written in a mainstream programming language.  Tool support for ASTRA is provided through a plugin for the Eclipse IDE.


