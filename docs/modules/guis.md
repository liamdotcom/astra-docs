Part 6: Example - Implementing GUI Support
==========================================

In this part of the guide, we explore how to link a GUI to an ASTRA agent using a module.  The proposed solution has been released as part of the the ASTRA-APIs artifact and can be used directly within your code.

Overview
--------

The basic idea behind this example is to define a standard way of linking agents and interfaces. The main challenge to be overcome is the creation of channels that allow GUI events to be passed to the agent and for the agent to send updates to the GUI.


<p style="font-weight:bold;text-align: center;">GUI &lt;----&gt; Agent</p>

The GUI -> Agent channel can be implemented using Custom Events.  This requires that the GUI code have a reference to the agent so that it can post events to the agents event queue. Conversly the Agent -> GUI channel can be implemented through a standardised method call. Because we want our solution to be generic, this can be implemented using a Java interface that must be implemented for each GUI.

There are two basic approaches for implementing this:

* **interface first**: For this approach, the agent is created by the GUI.
* **agent first**: Here, the agent creates the GUI.

Agent-First GUIs
----------------



