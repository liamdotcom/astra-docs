Creating Multiple Agents with ASTRA
===================================

This guide provides an overview of how to create a multi-agent system using ASTRA. A multi-agent system is a program that consists of multiple agents that execute concurrently in an autonomous fashion. In ASTRA, a multi-agent system is created by starting a single agent (known as the main agent). This agent then creates any additional agents as required. Those agents may, in turn, create additional agents leading to a hierarchical deployment with the main agent as the root.

What you’ll build
-----------------

You’ll build and deploy a simple application consisting of a self-replicating agent program. The idea is to demonstrate an agent can create another agent at run-time. The first agent will create another agent and pass it the number 1. That agent will create a second agent and pass it the number 2. This behaviour will continue until an agent is created and passed the number 5. This agent will stop the replication behaviour.

What you’ll need
----------------

* About 15 minutes
* A favorite text editor or IDE
* JDK 1.8 or later
* Maven 3.3+
* Ideally, you would have completed the [Building & Deploying ASTRA Projects](deploying.md) with Maven guide.

Creating a project from scratch
-------------------------------

In a project directory of your choosing, create the following directory structure (for example, on *nix systems, type `mkdir -p src/main/astra`):

```
|-- src
|    |-- main
|          |-- astra
|-- pom.xml
```

Now create the maven build file (pom.xml):

```
<project xmlns="http://maven.apache.org/POM/4.0.0"
		 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		 xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>examples</groupId>
	<artifactId>replicator</artifactId>
	<version>0.1.0</version>

    <parent>
        <groupId>com.astralanguage</groupId>
        <artifactId>astra-base</artifactId>
        <version>1.0.0</version>
    </parent>
    
    <build>
        <defaultGoal>clean compile astra:deploy</defaultGoal>
        <plugins>
            <plugin>
                <groupId>com.astralanguage</groupId>
                <artifactId>astra-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>
</project>
```

You should change the groupId and artifactId to reflect your own project.

Creating a project from an archetype
------------------------------------

To use the maven archtetype mechanism to create an ASTRA project, go to a folder of your choice, and type in the following command:

```
mvn archetype:generate \
	-DarchetypeGroupId=com.astralanguage \
	-DarchetypeArtifactId=astra-archetype \
	-DarchetypeVersion=1.0.0 \
	-DgroupId=examples \
	-DartifactId=replicator \
	-Dversion=0.1.0
```

You should change the groupId and artifactId to reflect your own project.

Learn what you can do with ASTRA
--------------------------------

The implementation of any multi-agent system requires mechanisms to allow the constituent agents to interact with one another. In general, this interaction is achieved through either the use of messaging infrastructure or shared resources. In this guide, we use neither. Instead, we use a simpler mechanism: when an agent creates another agent, it can give the agent it creates an initial `!main()`goal to initiate the behaviour of the created agent. In practice, this is not sufficient for interaction because the created agent has no way of contacting the creator.

Create a Replicator agent
-------------------------

In this example, a Replicator agent is an agent that is able to create another copy of itself. The agent code is given below:

`src/main/astra/Replicator.astra`

```
agent Replicator {
    module Console console;
    module System system;

    rule +!main([int value]) {
        console.println("Created agent with: " + value);
        if (value == 5) {
            console.println("Limit reached - replication stopped");
        } else {
	        system.createAgent("agt_"+value, "Replicator");
            system.setMainGoal("agt_"+value, [value+1]);
	    }
    }
}
```

There are a few things to note:

* An `if` statement is used to decide between 2 possible behaviours: if 5 replicator agents have been created, then the agent stops replicating; if less than 5 replicator agents have been created, then the agent creates another agent.
* The `System` API is used to create and initialise agents.
* The `createAgent(...)` action is used to create an agent. The first parameter is the name of the agent being created, and the second parameter is the type of agent (the agent program) to be created.
* The `setMainGoal(...)` action is used to give the newly created agent an initial goal to trigger its own behaviour. The first parameter is the name of the agent being created, and the second parameter is a list of values that will be used as the argument of the !main() goal.

Create a Main agent
-------------------

The Replicator agent program implements the behaviour we wanted but does not define how to start that behaviour. Later in this guide, we will demonstrate how to do this through customisation of the maven build process, but for now we will adopt a simpler approach – the creation of a `Main` agent. that is responsible for creating the first Replicator agent. The agent code is given below:

`src/main/astra/Main.astra`

```
agent Main {
    module System system;

    rule +!main(list args) {	    
        system.createAgent("agt_0", "Replicator");
        system.setMainGoal("agt_0", [1]);
    }
}
```

As can be seen, this agent uses the same functionality as the `Replicator` agent, with the exception that it uses hard-coded values for the name of the agent that it is going to create.

Run the application
-------------------

To run the application, simply type the following command in the project root folder:

```
mvn
```

The maven build file includes a default goal that is executed. You can do the same by entering the following line:

```
mvn clean compile astra:deploy
```

While there will be a lot of output, at the end you should see:

```
loading class: Replicator
[agt_0]Created agent with: 1
[agt_1]Created agent with: 2
[agt_2]Created agent with: 3
[agt_3]Created agent with: 4
[agt_4]Created agent with: 5
[agt_4]Limit reached - replication stopped
```

Summary
-------
Congratulations! You created and deployed your first multi-agent system using ASTRA and Maven. While the example is quite simple, you should be able to see that it is quite easy for an application to be deployed from a single `Main` agent. It is also important to recognise that, while this example creates only a single agent type, the approach can be used to create a diverse community of agents, which themselves can create more agents.

See Also
--------

The following guides may also be helpful:

* [Multi-Agent Communication with ASTRA](communication.md)
