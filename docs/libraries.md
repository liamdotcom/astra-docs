# 6. ASTRA API Library

The ASTRA API Library is a collection of ASTRA modules that provide a suite of basic functionaliy necessary to build multi-agent systems and can be found in [Gitlab](https://gitlab.com/astra-language/astra-core/-/blob/master/astra-apis/). This guide provides a brief overview of those modules

## 6.1 Using a module

To use an ASTRA module, you must first declare it via a `module` statement:

```
module <classname> <identifier>;
```

The `<classname>` parameter is a Java classname and can be either fully qualified (i.e. including the package) or just a classname.  In cases where a unqualified classname is used, you can also qualify the class through the use of an `import` statement. Unqualified classnames can also be used when the class is located in the `java.lang` or the `astra.lang` packages.  You will note that many of the modules provided in the ASTRA API library are located in the `astra.lang` package, meaning that you can use their unqualified classnames.

The `<identifier>` parameter is a unique name that is associated with an instance of the module (a module can have many instances, but the identifiers must be unique).  It is this identifier that is used in the main ASTRA code to reference the specific module you want to use, for example, to perform consoe output, you use the `astra.lang.Console` module:

```
module Console con;
```

Note that the identifier is not fixed and you can use anything you want. A common convention is to use a lowercase version of the classname as this has been shown to improve the readability of the code. However, this only works when you have a single instance of the module. In cases where you want to use multiple instances of a given module, you will need to be more inventive.

```
module Console console;
```

Once declared you can then use the module in your code. For example, to print out 'Hello World' using the first module declaration, you would write `con.println("Hello World");`. Alternatively, to use the second module declaration, you would write: `console.println("Hello World");`

## 6.2 astra.lang modules

The list below are the main modules provided to support programming with ASTRA.

### 6.2.1 astra.lang.Console

This module provides support for console input / output.  They are based around the System.in and System.out objects provided by Java.  The full class can be found [here](https://gitlab.com/astra-language/astra-core/-/blob/master/astra-apis/src/main/java/astra/lang/Console.java).

You declare an instance of the module as follows:

```
module Console console;
```

You can write to the console using `println(....)` actions:

```
console.println("Hello World");
```

You can read from the console using the `readXXX(...)` actions:

```
console.readString(string line);
```

Note that, for reading, you must pass an unbound variable to the action which will return the value entered by the user by binding it to the variable.

A simple example:

```
agent Example {
    module Console console;

    rule +!main(list args) {
        console.println("Please enter your name: ");
        console.read(string name);
        console.println("Hello, " + name);
    }
}
```

A typical output of this program would be:

```
Please enter your name:
Arthur
Hello, Arthur
```

### 6.2.2 astra.lang.Debug

This module provides some tools to help you debug your programs. The full class can be found [here](https://gitlab.com/astra-language/astra-core/-/blob/master/astra-apis/src/main/java/astra/lang/Debug.java).

You declare an instance of the module as follows:

```
module Debug debug;
```

You can print out all the agents current beliefs using the action:

```
debug.dumpBeliefs();
```

You can filter the beliefs by a predicate using the action:

```
debug.dumpBeliefsWithPredicate("is");
```

You can print out your intention stack using the action:

```
debug.printStackTrace();
```

An finally, you can print out the agents event queue with the action:

```
debug.printEventQueue();
```

A simple example:

```
agent Example {
    types eg {
        formula likes(string);
        formula has(string);
    }
    
    initial likes("icecream"), likes("sausages"), likes("beer");
    initial has("icecream"), has("pickles");

    module Debug debug;

    rule +!main(list args) {
        debug.dumpBeliefs();
        debug.dumpBeliefsWithPredicate("has");
    }
}
```

A typical output of this program would be:

```
----------------------------------------------------------------------------------------------
BELIEF DUMP FOR: main
    likes("icecream")
    likes("sausages")
    likes("beer")
    has("icecream")
    has("pickles")
----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------
BELIEF DUMP FOR: main
    has("icecream")
    has("pickles")
----------------------------------------------------------------------------------------------
```

### 6.2.3 astra.lang.Functions

### 6.2.4 astra.lang.Logic

### 6.2.5 astra.lang.Math

### 6.2.6 astra.lang.Messaging

### 6.2.7 astra.lang.ObjectAccess

### 6.2.8 astra.lang.Prelude

### 6.2.9 astra.lang.Reflection

### 6.2.10 astra.lang.Strings

### 6.2.11 astra.lang.System
