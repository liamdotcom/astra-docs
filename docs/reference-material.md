#7. Reference Material

##7.1 Additional Material
* [ASTRA Source Code](https://gitlab.com/astra-language/astra-core)
* [Maven Central](https://mvnrepository.com/artifact/com.astralanguage)
* [ACRE Guide](http://astralanguage.com/wordpress/acre-guide/)

##7.2 Old Guides
A number of earlier guides have been written for ASTRA. You can access them through the links below, but be aware that they are based on earlier prototypes of ASTRA and code snippets may not compile with later versions.

* [ASTRA Guide v0.1](http://astralanguage.com/wordpress/astra-guide/)
* [ASTRA Guide v0.2](http://astralanguage.com/wordpress/astra-user-guide/)
* [ASTRA Guide v0.3](http://astralanguage.com/wordpress/quick-guides/)
